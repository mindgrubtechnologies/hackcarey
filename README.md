# README #

### What is this repository for? ###

Hosting the code and environment settings for the 2015 Baltimore City Public Schools Hackathon

### How do I get set up? ###

* Go to [Cloud9](https://c9.io/web/sign-up/free)
* Sign up for a free account
* click "Create a new workspace"
* give your workspace a title
* decide if this workspace is public or private
* fill the "Clone from Git or Mercurial URL" field with "https://bitbucket.org/mindgrubtechnologies/hackathon_2015.git"
* click "Create workspace"
* go back to your dashboard by clicking the cloud icon in the upper right
* re open your workspace
